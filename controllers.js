angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {
           // alert("fuck");
           // var $scope = this;
            
            // data
            $scope.head = {
            a: "Name",
            b: "Surname",
            c: "City"
            };
            $scope.body = [{
                          a: "Hans",
                          b: "Mueller",
                          c: "Leipzig"
                          }, {
                          a: "Dieter",
                          b: "Zumpe",
                          c: "Berlin"
                          }, {
                          a: "Bernd",
                          b: "Danau",
                          c: "Muenchen"
                          }];
            
            $scope.sort = {
            column: '',
            descending: false
            };
            
            $scope.selectedCls = function(column) {
            return column == $scope.sort.column && 'sort-' + $scope.sort.descending;
            };
            
            $scope.changeSorting = function(column) {
            var sort = $scope.sort;
            if (sort.column == column) {
            sort.descending = !sort.descending;
            } else {
            sort.column = column;
            sort.descending = false;
            }
            };
            
    })

.controller('ChatsCtrl', function($scope, Chats) {
            // With the new view caching in Ionic, Controllers are only called
            // when they are recreated or on app start, instead of every page change.
            // To listen for when this page is active (for example, to refresh data),
            // listen for the $ionicView.enter event:
            //
            //$$scope.$on('$ionicView.enter', function(e) {
            //});
            
            $scope.chats = Chats.all();
            $scope.remove = function(chat) {
            Chats.remove(chat);
            };
            })

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
            $scope.chat = Chats.get($stateParams.chatId);
            })

.controller('AccountCtrl', function($scope) {
            $scope.settings = {
            enableFriends: true
            };
            })



